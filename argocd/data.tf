// get the remote state data for eks
data "terraform_remote_state" "eks" {
  backend = "s3"

  config = {
    bucket               = "solaris2451-aws-terraform-eks-with-argocd-state-bucket"
    key                  = "argocdinfra.json"
    region               = "us-east-1"
    workspace_key_prefix = "environment"
  }
}
